import pytest
from easyansi import colors_rgb as c
from easyansi._core.codes import CSI


####################################################################################################
# color, color_code
####################################################################################################


def test_color_code_no_selection():
    expected_msg = "No RGB color values provided"
    with pytest.raises(ValueError) as vl_error:
        c.color_code(None, None, None, None, None, None)
    assert expected_msg == str(vl_error.value)


@pytest.mark.parametrize(
    "value",
    [(c.MIN_COLOR - 1),
     (c.MAX_COLOR + 1),
     (None)])
def test_color_code_invalid_foreground_red(value):
    expected_msg_contains = "RGB Red Foreground Value"
    with pytest.raises(ValueError) as vl_error:
        c.color_code(value, 0, 0)
    assert expected_msg_contains in str(vl_error.value)


@pytest.mark.parametrize(
    "value",
    [(c.MIN_COLOR - 1),
     (c.MAX_COLOR + 1),
     (None)])
def test_color_code_invalid_foreground_green(value):
    expected_msg_contains = "RGB Green Foreground Value"
    with pytest.raises(ValueError) as vl_error:
        c.color_code(0, value, 0)
    assert expected_msg_contains in str(vl_error.value)


@pytest.mark.parametrize(
    "value",
    [(c.MIN_COLOR - 1),
     (c.MAX_COLOR + 1),
     (None)])
def test_color_code_invalid_foreground_blue(value):
    expected_msg_contains = "RGB Blue Foreground Value"
    with pytest.raises(ValueError) as vl_error:
        c.color_code(0, 0, value)
    assert expected_msg_contains in str(vl_error.value)


@pytest.mark.parametrize(
    "value",
    [(c.MIN_COLOR - 1),
     (c.MAX_COLOR + 1),
     (None)])
def test_color_code_invalid_background_red(value):
    expected_msg_contains = "RGB Red Background Value"
    with pytest.raises(ValueError) as vl_error:
        c.color_code(0, 0, 0, value, 0, 0)
    assert expected_msg_contains in str(vl_error.value)


@pytest.mark.parametrize(
    "value",
    [(c.MIN_COLOR - 1),
     (c.MAX_COLOR + 1),
     (None)])
def test_color_code_invalid_background_green(value):
    expected_msg_contains = "RGB Green Background Value"
    with pytest.raises(ValueError) as vl_error:
        c.color_code(0, 0, 0, 0, value, 0)
    assert expected_msg_contains in str(vl_error.value)


@pytest.mark.parametrize(
    "value",
    [(c.MIN_COLOR - 1),
     (c.MAX_COLOR + 1),
     (None)])
def test_color_code_invalid_background_blue(value):
    expected_msg_contains = "RGB Blue Background Value"
    with pytest.raises(ValueError) as vl_error:
        c.color_code(0, 0, 0, 0, 0, value)
    assert expected_msg_contains in str(vl_error.value)


def test_color_and_color_code_foreground(capsys):
    actual_code = c.color_code(3, 3, 3)
    c.color(3, 3, 3)
    captured = capsys.readouterr()
    assert actual_code == captured.out


def test_color_and_color_code_background(capsys):
    actual_code = c.color_code(None, None, None, 4, 4, 4)
    c.color(None, None, None, 4, 4, 4)
    captured = capsys.readouterr()
    assert actual_code == captured.out


def test_color_and_color_code_foreground_and_background(capsys):
    actual_code = c.color_code(1, 1, 1, 2, 2, 2)
    c.color(1, 1, 1, 2, 2, 2)
    captured = capsys.readouterr()
    assert actual_code == captured.out


def test_actual_color_code():
    r = 1
    g = 2
    b = 3
    fg_code = CSI + "38;2;" + str(r) + ";" + str(g) + ";" + str(b) + "m"
    bg_code = CSI + "48;2;" + str(r) + ";" + str(g) + ";" + str(b) + "m"
    assert c.color_code(r, g, b) == fg_code
    assert c.color_code(None, None, None, r, g, b) == bg_code
    assert c.color_code(r, g, b, r, g, b) == fg_code + bg_code
