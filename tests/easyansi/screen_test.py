import pytest
from easyansi import screen as s
from easyansi import cursor as cur
from easyansi._core.codes import CSI


####################################################################################################
# clear, clear_code
####################################################################################################


def test_clear_and_clear_code(capsys):
    actual_code = s.clear_code()
    s.clear()
    captured = capsys.readouterr()
    assert actual_code == captured.out


def test_clear_code():
    expected_code = CSI + "2J" + cur.locate_code(0, 0)
    actual_code = s.clear_code()
    assert expected_code == actual_code


####################################################################################################
# clear_line, clear_line_code
####################################################################################################


def test_clear_line_invalid_row():
    expected_msg_contains = "Row Number"
    with pytest.raises(ValueError) as vl_error:
        s.clear_line(s.MINIMUM_ROW - 1)
    assert expected_msg_contains in str(vl_error.value)


def test_clear_line_and_clear_line_code(capsys):
    row = 10
    actual_code = s.clear_line_code(row)
    s.clear_line(row)
    captured = capsys.readouterr()
    assert actual_code == captured.out


def test_clear_line_code():
    row = 10
    expected_code = cur.locate_code(0, row) + CSI + "2K"
    actual_code = s.clear_line_code(row)
    assert expected_code == actual_code


####################################################################################################
# get_size
####################################################################################################


def test_get_size():
    expected_cols = 80
    expected_rows = 24
    actual_cols, actual_rows = s.get_size()
    assert expected_cols == actual_cols
    assert expected_rows == actual_rows
