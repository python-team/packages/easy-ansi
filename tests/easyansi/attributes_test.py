import pytest
from easyansi import attributes as a
from easyansi._core.codes import CSI


def test_normal(capsys):
    ansi_code = a.normal_code()
    a.normal()
    captured = capsys.readouterr()
    assert ansi_code == captured.out


def test_bright(capsys):
    ansi_code = a.bright_code()
    a.bright()
    captured = capsys.readouterr()
    assert ansi_code == captured.out


def test_dim(capsys):
    ansi_code = a.dim_code()
    a.dim()
    captured = capsys.readouterr()
    assert ansi_code == captured.out


def test_underline(capsys):
    ansi_code = a.underline_code()
    a.underline()
    captured = capsys.readouterr()
    assert ansi_code == captured.out


def test_underline_off(capsys):
    ansi_code = a.underline_off_code()
    a.underline_off()
    captured = capsys.readouterr()
    assert ansi_code == captured.out


def test_italic(capsys):
    ansi_code = a.italic_code()
    a.italic()
    captured = capsys.readouterr()
    assert ansi_code == captured.out


def test_italic_off(capsys):
    ansi_code = a.italic_off_code()
    a.italic_off()
    captured = capsys.readouterr()
    assert ansi_code == captured.out


def test_blink(capsys):
    ansi_code = a.blink_code()
    a.blink()
    captured = capsys.readouterr()
    assert ansi_code == captured.out


def test_blink_off(capsys):
    ansi_code = a.blink_off_code()
    a.blink_off()
    captured = capsys.readouterr()
    assert ansi_code == captured.out


def test_reverse(capsys):
    ansi_code = a.reverse_code()
    a.reverse()
    captured = capsys.readouterr()
    assert ansi_code == captured.out


def test_reverse_off(capsys):
    ansi_code = a.reverse_off_code()
    a.reverse_off()
    captured = capsys.readouterr()
    assert ansi_code == captured.out


def test_conceal(capsys):
    ansi_code = a.conceal_code()
    a.conceal()
    captured = capsys.readouterr()
    assert ansi_code == captured.out


def test_conceal_off(capsys):
    ansi_code = a.conceal_off_code()
    a.conceal_off()
    captured = capsys.readouterr()
    assert ansi_code == captured.out


def test_strikethrough(capsys):
    ansi_code = a.strikethrough_code()
    a.strikethrough()
    captured = capsys.readouterr()
    assert ansi_code == captured.out


def test_strikethrough_off(capsys):
    ansi_code = a.strikethrough_off_code()
    a.strikethrough_off()
    captured = capsys.readouterr()
    assert ansi_code == captured.out


@pytest.mark.parametrize(
    "value,ansi_code,normal",
    [(a.normal_code(), "22", False),
     (a.bright_code(), "1", True),
     (a.dim_code(), "2", True),
     (a.underline_code(), "4", False),
     (a.underline_off_code(), "24", False),
     (a.italic_code(), "3", False),
     (a.italic_off_code(), "23", False),
     (a.blink_code(), "5", False),
     (a.blink_off_code(), "25", False),
     (a.reverse_code(), "7", False),
     (a.reverse_off_code(), "27", False),
     (a.conceal_code(), "8", False),
     (a.conceal_off_code(), "28", False),
     (a.strikethrough_code(), "9", False),
     (a.strikethrough_off_code(), "29", False)
     ])
def test_actual_codes(value, ansi_code, normal):
    code = CSI + ansi_code + "m"
    if normal:
        code = a.normal_code() + code
    assert value == code
