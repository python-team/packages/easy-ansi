import pytest

from easyansi._core.booleans import *
from easyansi._core.codes import CSI
from easyansi._core.colors import common_colors as c


####################################################################################################
# reset, reset_code
####################################################################################################


def test_reset_code():
    assert c._RESET == c.reset_code()


def test_reset(capsys):
    c.reset()
    captured = capsys.readouterr()
    assert c._RESET == captured.out


def test_reset_and_reset_code(capsys):
    c.reset()
    captured = capsys.readouterr()
    assert captured.out == c.reset_code()


def test_actual_reset_code():
    assert c.reset_code() == CSI + "0m"


####################################################################################################
# default, default_code
####################################################################################################


def test_default_code_no_flags():
    expected_msg = "Neither foreground or background default was requested."
    with pytest.raises(ValueError) as vl_error:
        c.default_code(None, None)
    assert expected_msg == str(vl_error.value)


def test_default_code_invalid_foreground_flag():
    expected_msg_contains = "Foreground Flag"
    with pytest.raises(ValueError) as vl_error:
        c.default_code(2, 0)
    assert expected_msg_contains in str(vl_error.value)


def test_default_code_invalid_background_flag():
    expected_msg_contains = "Background Flag"
    with pytest.raises(ValueError) as vl_error:
        c.default_code(0, 2)
    assert expected_msg_contains in str(vl_error.value)


def test_default_code_foreground():
    assert c._DEFAULT_FG == c.default_code(1, 0)
    assert c._DEFAULT_FG == c.default_code(TRUE, FALSE)


def test_default_code_background():
    assert c._DEFAULT_BG == c.default_code(0, 1)
    assert c._DEFAULT_BG == c.default_code(FALSE, TRUE)


def test_default_code_foreground_and_background():
    assert c._DEFAULT_FG_AND_BG == c.default_code()
    assert c._DEFAULT_FG_AND_BG == c.default_code(1, 1)
    assert c._DEFAULT_FG_AND_BG == c.default_code(TRUE, TRUE)


def test_default_foreground(capsys):
    c.default(1, 0)
    captured = capsys.readouterr()
    assert c._DEFAULT_FG == captured.out


def test_default_background(capsys):
    c.default(0, 1)
    captured = capsys.readouterr()
    assert c._DEFAULT_BG == captured.out


def test_default_foreground_and_background(capsys):
    c.default()
    captured = capsys.readouterr()
    assert c._DEFAULT_FG_AND_BG == captured.out
    c.default(1, 1)
    captured = capsys.readouterr()
    assert c._DEFAULT_FG_AND_BG == captured.out
    c.default(TRUE, TRUE)
    captured = capsys.readouterr()
    assert c._DEFAULT_FG_AND_BG == captured.out


def test_default_and_default_code(capsys):
    c.default()
    captured = capsys.readouterr()
    assert captured.out == c.default_code()
    assert captured.out == c.default_code(1, 1)
    assert captured.out == c.default_code(TRUE, TRUE)
    c.default(1, 1)
    captured = capsys.readouterr()
    assert captured.out == c.default_code()
    assert captured.out == c.default_code(1, 1)
    assert captured.out == c.default_code(TRUE, TRUE)
    c.default(1, 0)
    captured = capsys.readouterr()
    assert captured.out == c.default_code(1, 0)
    assert captured.out == c.default_code(TRUE, FALSE)
    c.default(0, 1)
    captured = capsys.readouterr()
    assert captured.out == c.default_code(0, 1)
    assert captured.out == c.default_code(FALSE, TRUE)


@pytest.mark.parametrize(
    "fg,bg,expected_code",
    [(1, 0, CSI + "39m"),
     (0, 1, CSI + "49m"),
     (1, 1, CSI + "39;49m")])
def test_actual_default_codes(fg, bg, expected_code):
    assert c.default_code(fg=fg, bg=bg) == expected_code
