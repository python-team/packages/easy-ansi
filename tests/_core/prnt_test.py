from easyansi._core.prnt import prnt


def test_prnt(capsys):
    text = "Hello World"
    prnt(text)
    captured = capsys.readouterr()
    assert text == captured.out
