"""Demo of using cursor codes to create animations."""

from easyansi import colors as c
from easyansi import screen as s
from easyansi import cursor as cur
from easyansi import drawing as d
from easyansi.screen import prnt
from time import sleep

DEFAULT_COLOR = c.color_code(c.WHITE, c.BLACK)
HEADING_COLOR = c.color_code(c.BRIGHT_YELLOW, c.BLACK)
SUB_HEADING_COLOR = c.color_code(c.BRIGHT_RED, c.BLACK)
PAGE_NUM_COLOR = c.color_code(c.BRIGHT_GREEN, c.BLACK)
LINE_COLOR = c.color_code(c.BRIGHT_BLUE, c.BLACK)
FOOTER_COLOR = c.color_code(c.BRIGHT_WHITE, c.BLACK)
SCREEN_WIDTH, SCREEN_HEIGHT = s.get_size()
HEADING = "Animations Demo"
TOTAL_PAGES = 1


def main():
    try:
        animations_demo()
    except KeyboardInterrupt:
        # The user pressed Ctrl+C to get here
        pass
    finally:
        # Return the terminal to the users default state
        cur.show()
        c.reset()
    print("\nCompleted")


def pause():
    cur.locate(0, SCREEN_HEIGHT - 1)
    prnt(FOOTER_COLOR)
    input("Press ENTER to continue, or CTRL+C to quit ")


def clear_screen(sub_heading, page_num):
    global SCREEN_WIDTH, SCREEN_HEIGHT
    SCREEN_WIDTH, SCREEN_HEIGHT = s.get_size()
    prnt(DEFAULT_COLOR)
    # When you clear the screen, it will set the whole screen to the current
    # foreground and background color that is currently set.
    s.clear()
    prnt(HEADING_COLOR + HEADING + ": ")
    prnt(SUB_HEADING_COLOR + sub_heading)
    page_text = str(page_num) + " / " + str(TOTAL_PAGES)
    cur.locate(SCREEN_WIDTH - len(page_text), 0)
    prnt(PAGE_NUM_COLOR + page_text)
    cur.locate(0, 1)
    prnt(LINE_COLOR)
    d.hline(SCREEN_WIDTH)
    cur.locate(0, SCREEN_HEIGHT - 2)
    d.hline(SCREEN_WIDTH)


def animations_demo():
    cur.hide()
    clear_screen("Watch the Fun", 1)
    draw_star_spiral(15, 10)
    sleep(1)
    draw_star_burst(40, 10)
    sleep(1)
    draw_star_clock(65, 10)
    sleep(1)
    cur.show()
    pause()


def draw_star_spiral(x, y):
    """Draw the star in a spiral pattern."""
    center_color = c.BRIGHT_CYAN
    cross_color = c.BRIGHT_MAGENTA
    x_color = c.BRIGHT_GREEN
    label_color = c.CYAN
    speed = 0.1

    # Label the star
    cur.locate(x - 2, y + 7)
    c.color(label_color)
    prnt("Spiral")
    cur.locate(x - 3, y + 8)
    prnt("Pattern")

    # Draw the star
    cur.locate(x, y)
    c.color(center_color)
    slow_prnt("*", speed)
    for i in range(0, 5):
        # right
        c.color(cross_color)
        slow_prnt("*", speed)
        # bottom right
        cur.down(1 + i)
        cur.left(1)
        c.color(x_color)
        slow_prnt("*", speed)
        # bottom
        cur.left(2 + i)
        c.color(cross_color)
        slow_prnt("*", speed)
        # bottom left
        cur.left(2 + i)
        c.color(x_color)
        slow_prnt("*", speed)
        # left
        cur.up(1 + i)
        cur.left(1)
        c.color(cross_color)
        slow_prnt("*", speed)
        # upper left
        cur.up(1 + i)
        cur.left(1)
        c.color(x_color)
        slow_prnt("*", speed)
        # upper
        if i > 0:
            cur.right(i)
        c.color(cross_color)
        slow_prnt("*", speed)
        # upper right
        if i > 0:
            cur.right(i)
        c.color(x_color)
        slow_prnt("*", speed)
        # right
        cur.down(1 + i)


def draw_star_burst(x, y):
    """Draw the star in a burst pattern."""
    center_color = c.BRIGHT_YELLOW
    cross_color = c.BRIGHT_RED
    x_color = c.BRIGHT_BLUE
    label_color = c.CYAN

    # Label the star
    cur.locate(x - 2, y + 7)
    c.color(label_color)
    prnt("Burst")
    cur.locate(x - 3, y + 8)
    prnt("Pattern")

    # Draw the star
    cur.locate(x, y)
    c.color(center_color)
    slow_prnt("*")
    for i in range(0, 5):
        output = ""
        output += c.color_code(cross_color)
        # right
        output += cur.locate_code(x + i + 1, y)
        output += "*"
        # bottom
        output += cur.locate_code(x, y + i + 1)
        output += "*"
        # left
        output += cur.locate_code(x - i - 1, y)
        output += "*"
        # top
        output += cur.locate_code(x, y - i - 1)
        output += "*"
        # top right
        output += c.color_code(x_color)
        output += cur.locate_code(x + i + 1, y - i - 1)
        output += "*"
        # bottom right
        output += cur.locate_code(x + i + 1, y + i + 1)
        output += "*"
        # bottom left
        output += cur.locate_code(x - i - 1, y + i + 1)
        output += "*"
        # top left
        output += cur.locate_code(x - i - 1, y - i - 1)
        output += "*"
        slow_prnt(output)


def draw_star_clock(x, y):
    """Draw the star in a clock pattern."""
    center_color = c.BRIGHT_RED
    cross_color = c.BRIGHT_YELLOW
    x_color = c.BRIGHT_CYAN
    label_color = c.CYAN

    # Label the star
    cur.locate(x - 2, y + 7)
    c.color(label_color)
    prnt("Clock")
    cur.locate(x - 3, y + 8)
    prnt("Pattern")

    # Draw the star
    cur.locate(x, y)
    c.color(center_color)
    slow_prnt("*")
    # top
    output = c.color_code(cross_color)
    for i in range(0, 5):
        output += cur.locate_code(x, y - 1 - i)
        output += "*"
    slow_prnt(output)
    # top right
    output = c.color_code(x_color)
    for i in range(0, 5):
        output += cur.locate_code(x + 1 + i, y - 1 - i)
        output += "*"
    slow_prnt(output)
    # right
    output = c.color_code(cross_color)
    output += cur.locate_code(x + 1, y)
    output += "*" * 5
    slow_prnt(output)
    # bottom right
    output = c.color_code(x_color)
    for i in range(0, 5):
        output += cur.locate_code(x + 1 + i, y + 1 + i)
        output += "*"
    slow_prnt(output)
    # bottom
    output = c.color_code(cross_color)
    for i in range(0, 5):
        output += cur.locate_code(x, y + 1 + i)
        output += "*"
    slow_prnt(output)
    # bottom left
    output = c.color_code(x_color)
    for i in range(0, 5):
        output += cur.locate_code(x - 1 - i, y + 1 + i)
        output += "*"
    slow_prnt(output)
    # left
    output = c.color_code(cross_color)
    output += cur.locate_code(x - 5, y)
    output += "*" * 5
    slow_prnt(output)
    # top left
    output = c.color_code(x_color)
    for i in range(0, 5):
        output += cur.locate_code(x - 1 - i, y - 1 - i)
        output += "*"
    slow_prnt(output)


def slow_prnt(text, speed=0.3):
    prnt(text)
    sleep(speed)


if __name__ == "__main__":
    main()
