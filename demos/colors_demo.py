"""Demo of the 16 color palette."""

from easyansi import colors as c
from easyansi import screen as s
from easyansi import cursor as cur
from easyansi import drawing as d
from easyansi import attributes as a
from easyansi.screen import prnt

DEFAULT_COLOR = c.color_code(c.WHITE, c.BLACK)
HEADING_COLOR = c.color_code(c.BRIGHT_YELLOW, c.BLACK)
SUB_HEADING_COLOR = c.color_code(c.BRIGHT_RED, c.BLACK)
PAGE_NUM_COLOR = c.color_code(c.BRIGHT_GREEN, c.BLACK)
LINE_COLOR = c.color_code(c.BRIGHT_BLUE, c.BLACK)
FOOTER_COLOR = c.color_code(c.BRIGHT_WHITE, c.BLACK)
SCREEN_WIDTH, SCREEN_HEIGHT = s.get_size()
HEADING = "Standard Color Palette and Attributes Demo"
TOTAL_PAGES = 3


def main():
    try:
        colors_demo()
    except KeyboardInterrupt:
        # The user pressed Ctrl+C to get here
        pass
    finally:
        # Return the terminal to the users default state
        c.reset()
    print("\nCompleted")


def pause():
    cur.locate(0, SCREEN_HEIGHT - 1)
    prnt(FOOTER_COLOR)
    input("Press ENTER to continue, or CTRL+C to quit ")


def clear_screen(sub_heading, page_num):
    global SCREEN_WIDTH, SCREEN_HEIGHT
    SCREEN_WIDTH, SCREEN_HEIGHT = s.get_size()
    prnt(DEFAULT_COLOR)
    # When you clear the screen, it will set the whole screen to the current
    # foreground and background color that is currently set.
    s.clear()
    prnt(HEADING_COLOR + HEADING + ": ")
    prnt(SUB_HEADING_COLOR + sub_heading)
    page_text = str(page_num) + " / " + str(TOTAL_PAGES)
    cur.locate(SCREEN_WIDTH - len(page_text), 0)
    prnt(PAGE_NUM_COLOR + page_text)
    cur.locate(0, 1)
    prnt(LINE_COLOR)
    d.hline(SCREEN_WIDTH)
    cur.locate(0, SCREEN_HEIGHT - 2)
    d.hline(SCREEN_WIDTH)


def colors_demo():
    colors_demo_screen()
    pause()
    intensities_demo_screen()
    pause()
    attributes_demo_screen()
    pause()


def colors_demo_screen():
    clear_screen("Color Names and Indexes", 1)
    column_width = SCREEN_WIDTH // 8
    column_header_format = "{: ^" + str(column_width) + "s}"
    color_names = ("BLACK", "RED", "GREEN", "YELLOW", "BLUE", "MAGENTA", "CYAN", "WHITE")
    # Table 1 Description
    cur.locate(0, 3)
    prnt(DEFAULT_COLOR + "Modern terminals can show 16 colors in both foreground and background colors.")
    # Column Headings
    cur.locate(0, 5)
    for color_idx, color_name in enumerate(color_names):
        if color_idx == 0:
            c.color(color_idx, c.WHITE)
        else:
            c.color(color_idx, c.BLACK)
        prnt(column_header_format.format(color_name))
    # Column Headings Bright
    cur.locate(0, 6)
    for color_idx in range(8, 16):
        c.color(color_idx)
        prnt(column_header_format.format("BRIGHT"))
    # Color Cells Line 1
    color_cell_format = "{: ^" + str(column_width) + "d}"
    cur.locate(0, 8)
    for color_idx in range(0, 8):
        c.color(c.BLACK, color_idx)
        if color_idx == c.BLACK:
            c.color(c.WHITE)
        prnt(color_cell_format.format(color_idx))
    # Color Cells Line 2
    cur.locate(0, 9)
    for color_idx in range(8, 16):
        c.color(c.BLACK, color_idx)
        prnt(color_cell_format.format(color_idx))
    # Table 2 Description
    cur.locate(0, 12)
    prnt(DEFAULT_COLOR + "Older terminals can not display colors 8-15 as a background color, and use the")
    cur.locate(0, 13)
    prnt("bright (bold) attribute to simulate colors 8-15 as a foreground color.")
    # Color Cells Line 3
    cur.locate(0, 15)
    a.bright()
    for color_idx in range(0, 8):
        c.color(color_idx)
        prnt(column_header_format.format("BOLD"))
    a.normal()
    # Default Color
    cur.locate(0, 18)
    prnt(DEFAULT_COLOR + '"Default" is also a color, as set by your terminal.')
    cur.locate(0, 20)
    c.default(c.TRUE, c.TRUE)
    prnt("This text is shown in the default colors defined by your terminal.")


def intensities_demo_screen():
    number_format = "{: >4d}"
    clear_screen("Attributes - Intensities", 2)
    cur.locate(0, 2)
    prnt(DEFAULT_COLOR + "Color")
    cur.locate(0, 3)
    prnt("Index")
    for color_idx in range(0, 8):
        cur.locate(0, 4 + color_idx)
        c.color(c.WHITE)
        prnt(number_format.format(color_idx))
        prnt("   ")
        c.color(color_idx)
        a.dim()
        prnt("Dim")
        prnt("   ")
        a.normal()
        prnt("Normal")
        prnt("   ")
        a.bright()
        prnt("Bright")
        a.normal()
    cur.locate(0, 4 + 8)
    d.hline(29)
    for color_idx in range(8, 16):
        cur.locate(0, 5 + color_idx)
        c.color(c.WHITE)
        prnt(number_format.format(color_idx))
        prnt("   ")
        c.color(color_idx)
        a.dim()
        prnt("Dim")
        prnt("   ")
        a.normal()
        prnt("Normal")
        prnt("   ")
        a.bright()
        prnt("Bright")
        a.normal()
    # Description of screen
    cur.locate(35, 5)
    prnt(DEFAULT_COLOR)
    prnt("This screen shows each of the 16 colors")
    cur.locate(35, 6)
    prnt("and how they look with each of the 3")
    cur.locate(35, 7)
    prnt("intensity attributes applied.")
    cur.locate(35, 9)
    prnt("Attributes only apply to foreground")
    cur.locate(35, 10)
    prnt("colors.")


def attributes_demo_screen():
    clear_screen("The Other Attributes", 3)
    table_heading_color = c.color_code(c.CYAN, c.BLACK)
    table_separator_color = c.color_code(c.YELLOW, c.BLACK)
    label_color = c.color_code(c.CYAN, c.BLACK)
    text_color = c.color_code(c.WHITE, c.BLACK)
    col_0 = 0
    col_1 = 16
    col_2 = 30

    def write_attributes_detail_line(label, code_on, code_off, row):
        cur.locate(col_0, row)
        prnt(label_color + label)
        cur.locate(col_1, row)
        prnt(text_color)
        prnt(code_on)
        prnt("Sample Text")
        prnt(code_off)
        cur.locate(col_2, row)
        prnt("Sample Text")

    # Table Heading
    row = 3
    cur.locate(col_0, row)
    prnt(table_heading_color + "Attribute")
    cur.locate(col_1, row)
    prnt("On")
    cur.locate(col_2, row)
    prnt("Off")
    row = 4
    cur.locate(col_0, row)
    prnt(table_separator_color)
    d.hline(41)
    # Attributes
    write_attributes_detail_line("Underline", a.underline_code(), a.underline_off_code(), 6)
    write_attributes_detail_line("Italic", a.italic_code(), a.italic_off_code(), 8)
    write_attributes_detail_line("Reverse", a.reverse_code(), a.reverse_off_code(), 10)
    write_attributes_detail_line("Strikethrough", a.strikethrough_code(), a.strikethrough_off_code(), 12)
    write_attributes_detail_line("Blink", a.blink_code(), a.blink_off_code(), 14)
    write_attributes_detail_line("Conceal", a.conceal_code(), a.conceal_off_code(), 16)
    # Page Description
    prnt(DEFAULT_COLOR)
    cur.locate(45, 5)
    prnt("NOTE:")
    cur.locate(45, 7)
    prnt(DEFAULT_COLOR)
    prnt("Not all terminals can display all")
    cur.locate(45, 8)
    prnt("attribute types.")


if __name__ == "__main__":
    main()
