"""Demo of the RGB color palette."""

from easyansi import colors_rgb as c
from easyansi import screen as s
from easyansi import cursor as cur
from easyansi import drawing as d
from easyansi.screen import prnt
from random import randint

DEFAULT_COLOR = c.color_code(170, 170, 170, 0, 0, 0)
HEADING_COLOR = c.color_code(255, 255, 85, 0, 0, 0)
SUB_HEADING_COLOR = c.color_code(255, 85, 85, 0, 0, 0)
PAGE_NUM_COLOR = c.color_code(85, 255, 85, 0, 0, 0)
LINE_COLOR = c.color_code(85, 85, 255, 0, 0, 0)
FOOTER_COLOR = c.color_code(255, 255, 255, 0, 0, 0)
SCREEN_WIDTH, SCREEN_HEIGHT = s.get_size()
HEADING = "RGB Color Palette Demo"
TOTAL_PAGES = 4


def main():
    try:
        colors_rgb_demo()
    except KeyboardInterrupt:
        # The user pressed Ctrl+C to get here
        pass
    finally:
        # Return the terminal to the users default state
        c.reset()
    print("\nCompleted")


def pause():
    cur.locate(0, SCREEN_HEIGHT - 1)
    prnt(FOOTER_COLOR)
    input("Press ENTER to continue, or CTRL+C to quit ")


def clear_screen(sub_heading, page_num):
    global SCREEN_WIDTH, SCREEN_HEIGHT
    SCREEN_WIDTH, SCREEN_HEIGHT = s.get_size()
    prnt(DEFAULT_COLOR)
    # When you clear the screen, it will set the whole screen to the current
    # foreground and background color that is currently set.
    s.clear()
    prnt(HEADING_COLOR + HEADING + ": ")
    prnt(SUB_HEADING_COLOR + sub_heading)
    page_text = str(page_num) + " / " + str(TOTAL_PAGES)
    cur.locate(SCREEN_WIDTH - len(page_text), 0)
    prnt(PAGE_NUM_COLOR + page_text)
    cur.locate(0, 1)
    prnt(LINE_COLOR)
    d.hline(SCREEN_WIDTH)
    cur.locate(0, SCREEN_HEIGHT - 2)
    d.hline(SCREEN_WIDTH)


def colors_rgb_demo():
    print_color_chart("Red", 1)
    pause()
    print_color_chart("Green", 2)
    pause()
    print_color_chart("Blue", 3)
    pause()
    print_random_colors()
    pause()


def print_color_chart(color_name, page_num):
    number_format = "{: >3}"
    clear_screen(color_name + " Colors", page_num)
    cur.locate(0, 3)
    prnt(DEFAULT_COLOR + "Foreground Colors:")
    for row in range(0, 4):
        row_start = row * 64
        cur.locate(0, 5 + row)
        prnt(DEFAULT_COLOR)
        prnt(number_format.format(row_start))
        prnt("  ")
        for col in range(0, 64):
            color_idx = col + row_start
            if color_name == "Red":
                c.color(color_idx, 0, 0, 0, 0, 0)
            elif color_name == "Green":
                c.color(0, color_idx, 0, 0, 0, 0)
            elif color_name == "Blue":
                c.color(0, 0, color_idx, 0, 0, 0)
            prnt("#")
        prnt(DEFAULT_COLOR)
        prnt("  ")
        prnt(number_format.format((row * 64) + 63))
    cur.locate(0, 11)
    prnt(DEFAULT_COLOR + "Background Colors:")
    for row in range(0, 4):
        row_start = row * 64
        cur.locate(0, 13 + row)
        prnt(DEFAULT_COLOR)
        prnt(number_format.format(row_start))
        prnt("  ")
        for col in range(0, 64):
            color_idx = col + row_start
            if color_name == "Red":
                c.color(0, 0, 0, color_idx, 0, 0)
            elif color_name == "Green":
                c.color(0, 0, 0, 0, color_idx, 0)
            elif color_name == "Blue":
                c.color(0, 0, 0, 0, 0, color_idx)
            prnt(" ")
        prnt(DEFAULT_COLOR)
        prnt("  ")
        prnt(number_format.format((row * 64) + 63))


def print_random_colors():
    clear_screen("Just for Fun - Random Colors and Characters", 4)
    num_rows = SCREEN_HEIGHT - 4
    num_cols = SCREEN_WIDTH
    chars_to_print = "abcdefghijklmnopqrstuvwxyz"
    chars_to_print += "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    chars_to_print += "1234567890!@#$%^&*()"
    chars_to_print += "`~-_=+[{]}\\|;:'\",<.>/?"
    for row in range(0, num_rows):
        for col in range(0, num_cols):
            cur.locate(col, row + 2)
            fg_r = randint(0, 255)
            fg_g = randint(0, 255)
            fg_b = randint(0, 255)
            bg_r = randint(0, 255)
            bg_g = randint(0, 255)
            bg_b = randint(0, 255)
            char_idx = randint(0, len(chars_to_print) - 1)
            c.color(fg_r, fg_g, fg_b, bg_r, bg_g, bg_b)
            prnt(chars_to_print[char_idx:char_idx + 1])


if __name__ == "__main__":
    main()
