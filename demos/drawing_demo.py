"""Demo of the drawing capabilities."""

from easyansi import colors as c
from easyansi import screen as s
from easyansi import cursor as cur
from easyansi import drawing as d
from easyansi.screen import prnt

DEFAULT_COLOR = c.color_code(c.WHITE, c.BLACK)
HEADING_COLOR = c.color_code(c.BRIGHT_YELLOW, c.BLACK)
SUB_HEADING_COLOR = c.color_code(c.BRIGHT_RED, c.BLACK)
PAGE_NUM_COLOR = c.color_code(c.BRIGHT_GREEN, c.BLACK)
LINE_COLOR = c.color_code(c.BRIGHT_BLUE, c.BLACK)
FOOTER_COLOR = c.color_code(c.BRIGHT_WHITE, c.BLACK)
SCREEN_WIDTH, SCREEN_HEIGHT = s.get_size()
HEADING = "Drawing Demo"
TOTAL_PAGES = 3


def main():
    try:
        drawing_demo()
    except KeyboardInterrupt:
        # The user pressed Ctrl+C to get here
        pass
    finally:
        # Return the terminal to the users default state
        c.reset()
    print("\nCompleted")


def pause():
    cur.locate(0, SCREEN_HEIGHT - 1)
    prnt(FOOTER_COLOR)
    input("Press ENTER to continue, or CTRL+C to quit ")


def clear_screen(sub_heading, page_num):
    global SCREEN_WIDTH, SCREEN_HEIGHT
    SCREEN_WIDTH, SCREEN_HEIGHT = s.get_size()
    prnt(DEFAULT_COLOR)
    # When you clear the screen, it will set the whole screen to the current
    # foreground and background color that is currently set.
    s.clear()
    prnt(HEADING_COLOR + HEADING + ": ")
    prnt(SUB_HEADING_COLOR + sub_heading)
    page_text = str(page_num) + " / " + str(TOTAL_PAGES)
    cur.locate(SCREEN_WIDTH - len(page_text), 0)
    prnt(PAGE_NUM_COLOR + page_text)
    cur.locate(0, 1)
    prnt(LINE_COLOR)
    d.hline(SCREEN_WIDTH)
    cur.locate(0, SCREEN_HEIGHT - 2)
    d.hline(SCREEN_WIDTH)


def drawing_demo():
    hline_demo()
    pause()
    vline_demo()
    pause()
    box_demo()
    pause()


def hline_demo():
    clear_screen("hline", 1)
    label_color = c.color_code(c.CYAN)
    line_color = c.color_code(c.WHITE)
    cur.locate(0, 3)
    prnt(label_color + "Default Line:  ")
    print(line_color + d.hline_code(30))
    print()
    prnt(label_color + "Single Line:   ")
    print(line_color + d.hline_code(30, style=d.styles.SINGLE))
    print()
    prnt(label_color + "Double Line:   ")
    print(line_color + d.hline_code(30, style=d.styles.DOUBLE))
    print()
    prnt(label_color + "Keyboard Line: ")
    print(line_color + d.hline_code(30, style=d.styles.KEYBOARD))
    print()
    prnt(label_color + "Custom Line 1: ")
    print(line_color + d.hline_code(30, char="#"))
    print()
    prnt(label_color + "Custom Line 2: ")
    print(line_color + d.hline_code(30, char="<-> "))
    print()


def vline_demo():
    clear_screen("vline", 2)
    label_color = c.color_code(c.CYAN)
    line_color = c.color_code(c.WHITE)
    cur.locate(0, 3)
    print(label_color + "Default  Single  Double  Keyboard  Custom  Custom")
    print(label_color + " Line     Line    Line     Line    Line 1  Line 2")
    print(line_color)
    row_start = 6
    cur.locate(2, row_start)
    d.vline(15)
    cur.locate(11, row_start)
    d.vline(15, style=d.styles.SINGLE)
    cur.locate(19, row_start)
    d.vline(15, style=d.styles.DOUBLE)
    cur.locate(28, row_start)
    d.vline(15, style=d.styles.KEYBOARD)
    cur.locate(37, row_start)
    d.vline(15, char="I")
    cur.locate(45, row_start)
    d.vline(15, char="^|v ")
    cur.locate(0, 22)


def box_demo():
    clear_screen("Boxes", 3)
    label_color = c.color_code(c.WHITE)
    line_color = c.color_code(c.BRIGHT_GREEN)

    # Default Box
    x = 1
    y = 3
    cur.locate(x + 2, y + 1)
    prnt(label_color + "Default")
    cur.locate(x + 4, y + 2)
    prnt("Box")
    cur.locate(x, y)
    prnt(line_color)
    d.box(11, 4)

    # Single Box
    x = 15
    y = 3
    cur.locate(x + 2, y + 1)
    prnt(label_color + "Single")
    cur.locate(x + 3, y + 2)
    prnt("Box")
    cur.locate(x, y)
    prnt(line_color)
    d.box(10, 4, style=d.styles.SINGLE)

    # Double Box
    x = 28
    y = 3
    cur.locate(x + 2, y + 1)
    prnt(label_color + "Double")
    cur.locate(x + 3, y + 2)
    prnt("Box")
    cur.locate(x, y)
    prnt(line_color)
    d.box(10, 4, style=d.styles.DOUBLE)

    # Keyboard Box
    x = 41
    y = 3
    cur.locate(x + 2, y + 1)
    prnt(label_color + "Keyboard")
    cur.locate(x + 4, y + 2)
    prnt("Box")
    cur.locate(x, y)
    prnt(line_color)
    d.box(12, 4, style=d.styles.KEYBOARD)

    # Custom Box 1
    x = 1
    y = 9
    cur.locate(x + 2, y + 1)
    prnt(label_color + "Custom")
    cur.locate(x + 2, y + 2)
    prnt("Box 1")
    cur.locate(x, y)
    prnt(line_color)
    d.box(10, 4, top_bottom="~", left="[", right="]", all_corners="@")

    # Custom Box 2
    x = 14
    y = 9
    cur.locate(x + 2, y + 1)
    prnt(label_color + "Custom")
    cur.locate(x + 2, y + 2)
    prnt("Box 2")
    cur.locate(x, y)
    prnt(line_color)
    d.box(10, 4, top_left="/", top_right="\\", bottom_left="\\", bottom_right="/",
          top="=", bottom="-", left="{", right="}")

    # Custom Box 3
    x = 27
    y = 9
    cur.locate(x + 2, y + 1)
    prnt(label_color + "Custom")
    cur.locate(x + 2, y + 2)
    prnt("Box 3")
    cur.locate(x, y)
    prnt(line_color)
    d.box(10, 4, all_chars="*")


if __name__ == "__main__":
    main()
