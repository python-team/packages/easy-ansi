# Easy ANSI Demo Programs

## Instructions:

* Install Easy ANSI
* Set your terminal size no smaller than 80 columns wide by 24 rows high.
* Run each demo as a standard Python 3 program.

## Demos

| File Name | Description |
| --- | --- |
| animation_demo.py | Draws stars on the screen using cursor codes. |
| colors_demo.py | Shows the standard 16-color palette as well as the attributes. |
| colors_256_demo.py | Shows the 256-color palette. |
| colors_rgb_demo.py | Shows the 24-bit (RGB) color palette. |
| drawing_demo.py | Shows drawing lines and boxes. |
| screen_cursor_demo.py | Shows using screen and cursor commands |
