"""Demo of the 256 color palette."""

from easyansi import colors_256 as c
from easyansi import screen as s
from easyansi import cursor as cur
from easyansi import drawing as d
from easyansi.screen import prnt

DEFAULT_COLOR = c.color_code(c.WHITE, c.BLACK)
HEADING_COLOR = c.color_code(c.BRIGHT_YELLOW, c.BLACK)
SUB_HEADING_COLOR = c.color_code(c.BRIGHT_RED, c.BLACK)
PAGE_NUM_COLOR = c.color_code(c.BRIGHT_GREEN, c.BLACK)
LINE_COLOR = c.color_code(c.BRIGHT_BLUE, c.BLACK)
FOOTER_COLOR = c.color_code(c.BRIGHT_WHITE, c.BLACK)
SCREEN_WIDTH, SCREEN_HEIGHT = s.get_size()
HEADING = "256 Color Palette Demo"
TOTAL_PAGES = 3

COLOR_ROWS_1 = [16, 52, 88, 124, 160, 196]
COLOR_ROWS_2 = [34, 70, 106, 142, 178, 214]


def main():
    try:
        colors_256_demo()
    except KeyboardInterrupt:
        # The user pressed Ctrl+C to get here
        pass
    finally:
        # Return the terminal to the users default state
        c.reset()
    print("\nCompleted")


def pause():
    cur.locate(0, SCREEN_HEIGHT - 1)
    prnt(FOOTER_COLOR)
    input("Press ENTER to continue, or CTRL+C to quit ")


def clear_screen(sub_heading, page_num):
    global SCREEN_WIDTH, SCREEN_HEIGHT
    SCREEN_WIDTH, SCREEN_HEIGHT = s.get_size()
    prnt(DEFAULT_COLOR)
    # When you clear the screen, it will set the whole screen to the current
    # foreground and background color that is currently set.
    s.clear()
    prnt(HEADING_COLOR + HEADING + ": ")
    prnt(SUB_HEADING_COLOR + sub_heading)
    page_text = str(page_num) + " / " + str(TOTAL_PAGES)
    cur.locate(SCREEN_WIDTH - len(page_text), 0)
    prnt(PAGE_NUM_COLOR + page_text)
    cur.locate(0, 1)
    prnt(LINE_COLOR)
    d.hline(SCREEN_WIDTH)
    cur.locate(0, SCREEN_HEIGHT - 2)
    d.hline(SCREEN_WIDTH)


def colors_256_demo():
    colors_216_demo_screen(True)
    pause()
    colors_216_demo_screen(False)
    pause()
    colors_standard_and_grayscale_demo_screen()
    pause()


def colors_216_demo_screen(foreground):
    sub_heading = "216 Color Palette - "
    sub_heading += "Foreground" if foreground else "Background"
    page_num = 1 if foreground else 2
    clear_screen(sub_heading, page_num)
    for row_num, color_row_start in enumerate(COLOR_ROWS_1):
        cur.locate(0, row_num + 3)
        for col_num in range(0, 18):
            color_index = color_row_start + col_num
            _print_216_color(color_index, col_num, foreground)
    for row_num, color_row_start in enumerate(COLOR_ROWS_2):
        cur.locate(0, row_num + 10)
        for col_num in range(0, 18):
            color_index = color_row_start + col_num
            _print_216_color(color_index, col_num, foreground)


def _print_216_color(color_index, col_num, foreground):
    number_format = "{:0>3}"
    if col_num == 6 or col_num == 12:
        c.color(c.BLACK, c.BLACK)
        prnt("  ")
    if foreground:
        c.color(color_index, c.BLACK)
    else:
        c.color(c.BLACK, color_index)
    prnt(number_format.format(color_index))
    c.color(c.BLACK, c.BLACK)
    if col_num < 17:
        prnt(" ")


def colors_standard_and_grayscale_demo_screen():
    clear_screen("Color Names and Indexes", 3)
    column_width = SCREEN_WIDTH // 8
    column_header_format = "{: ^" + str(column_width) + "s}"
    color_names = ("BLACK", "RED", "GREEN", "YELLOW", "BLUE", "MAGENTA", "CYAN", "WHITE")
    # Table 1 Description
    cur.locate(0, 3)
    prnt(DEFAULT_COLOR + "The 256 color palette also has the standard 16 colors.")
    # Column Headings
    cur.locate(0, 5)
    for color_idx, color_name in enumerate(color_names):
        if color_name == "BLACK":
            c.color(c.BLACK, c.WHITE)
        else:
            c.color(color_idx, c.BLACK)
        prnt(column_header_format.format(color_name))
    # Column Headings Bright
    cur.locate(0, 6)
    for color_idx, color_name in enumerate(color_names):
        c.color(color_idx + 8, c.BLACK)
        prnt(column_header_format.format("BRIGHT"))
    # Color Cells Line 1
    color_cell_format = "{: ^" + str(column_width) + "d}"
    cur.locate(0, 8)
    for color_idx in range(0, 8):
        c.color(c.BLACK, color_idx)
        if color_idx == c.BLACK:
            c.color(c.WHITE)
        prnt(color_cell_format.format(color_idx))
    # Color Cells Line 2
    cur.locate(0, 9)
    for color_idx in range(8, 16):
        c.color(c.BLACK, color_idx)
        prnt(color_cell_format.format(color_idx))

    # Gray Scale Description
    cur.locate(0, 12)
    prnt(DEFAULT_COLOR + "In addition, 24 gray-scale colors are defined.")
    # Gray Line 1
    cur.locate(0, 14)
    for color_idx in range(0, 8):
        if color_idx == 0:
            c.color(color_idx + 232, c.WHITE)
        else:
            c.color(color_idx + 232, c.BLACK)
        prnt(column_header_format.format("GRAY" + str(color_idx)))
    # Gray Line 2
    cur.locate(0, 15)
    for color_idx in range(8, 16):
        c.color(color_idx + 232, c.BLACK)
        prnt(column_header_format.format("GRAY" + str(color_idx)))
    # Gray Line 3
    cur.locate(0, 16)
    for color_idx in range(16, 24):
        c.color(color_idx + 232, c.BLACK)
        prnt(column_header_format.format("GRAY" + str(color_idx)))
    # Gray Line 4
    cur.locate(0, 18)
    for color_idx in range(0, 8):
        color_index = color_idx + 232
        if color_idx == 0:
            c.color(c.WHITE, color_index)
        else:
            c.color(c.BLACK, color_index)
        prnt(color_cell_format.format(color_index))
    # Gray Line 5
    cur.locate(0, 19)
    for color_idx in range(8, 16):
        color_index = color_idx + 232
        c.color(c.BLACK, color_index)
        prnt(color_cell_format.format(color_index))
    # Gray Line 6
    cur.locate(0, 20)
    for color_idx in range(16, 24):
        color_index = color_idx + 232
        c.color(c.BLACK, color_index)
        prnt(color_cell_format.format(color_index))


if __name__ == "__main__":
    main()
