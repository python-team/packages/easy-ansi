"""Demo of screen and cursor commands."""

from easyansi import colors as c
from easyansi import screen as s
from easyansi import cursor as cur
from easyansi import drawing as d
from easyansi.screen import prnt

DEFAULT_COLOR = c.color_code(c.WHITE, c.BLACK)
HEADING_COLOR = c.color_code(c.BRIGHT_YELLOW, c.BLACK)
SUB_HEADING_COLOR = c.color_code(c.BRIGHT_RED, c.BLACK)
PAGE_NUM_COLOR = c.color_code(c.BRIGHT_GREEN, c.BLACK)
LINE_COLOR = c.color_code(c.BRIGHT_BLUE, c.BLACK)
FOOTER_COLOR = c.color_code(c.BRIGHT_WHITE, c.BLACK)
SCREEN_WIDTH, SCREEN_HEIGHT = s.get_size()
HEADING = "Screen and Cursor Commands Demo"
TOTAL_PAGES = 1


def main():
    try:
        screen_cursor_demo()
    except KeyboardInterrupt:
        # The user pressed Ctrl+C to get here
        pass
    finally:
        # Return the terminal to the users default state
        c.reset()
    print("\nCompleted")


def pause():
    cur.locate(0, SCREEN_HEIGHT - 1)
    prnt(FOOTER_COLOR)
    input("Press ENTER to continue, or CTRL+C to quit ")


def clear_screen(sub_heading, page_num):
    global SCREEN_WIDTH, SCREEN_HEIGHT
    SCREEN_WIDTH, SCREEN_HEIGHT = s.get_size()
    prnt(DEFAULT_COLOR)
    # When you clear the screen, it will set the whole screen to the current
    # foreground and background color that is currently set.
    s.clear()
    prnt(HEADING_COLOR + HEADING + ": ")
    prnt(SUB_HEADING_COLOR + sub_heading)
    page_text = str(page_num) + " / " + str(TOTAL_PAGES)
    cur.locate(SCREEN_WIDTH - len(page_text), 0)
    prnt(PAGE_NUM_COLOR + page_text)
    cur.locate(0, 1)
    prnt(LINE_COLOR)
    d.hline(SCREEN_WIDTH)
    cur.locate(0, SCREEN_HEIGHT - 2)
    d.hline(SCREEN_WIDTH)


def screen_cursor_demo():
    label_color = c.color_code(c.CYAN, c.BLACK)
    field_color = c.color_code(c.WHITE, c.BLACK)
    note_color = c.color_code(c.GREEN, c.BLACK)
    col_1 = 0
    col_2 = 39
    clear_screen("Main", 1)

    current_row = 2
    # Demo getting the screen size
    cur.locate(col_1, current_row)
    screen_width, screen_height = s.get_size()
    prnt(label_color + "Screen Size:")
    cur.locate(col_2, current_row)
    prnt(field_color + "x=" + str(screen_width) + ", y=" + str(screen_height))

    # Demo getting the cursor location
    current_row += 1
    cur.locate(39, 19)
    prnt(c.color_code(c.BRIGHT_YELLOW, c.BLACK))
    prnt("*")
    cur.left()
    cursor_x, cursor_y = cur.get_location()
    cur.locate(col_1, current_row)
    prnt(label_color + "Cursor Location at * :")
    cur.locate(col_2, current_row)
    prnt(field_color + "x=" + str(cursor_x) + ", y=" + str(cursor_y))
    prnt(note_color + "  (should be x=39, y=19)")

    # Demo clearing a line
    c.color(c.RED)
    cur.locate(0, 15)
    d.hline(80, char="=")
    cur.locate(0, 18)
    d.hline(80, char="=")
    cur.locate(0, 16)
    c.color(c.WHITE)
    prnt("You should not see this text.")
    cur.locate(0, 17)
    prnt("You should also not see this text.")
    cur.locate(40, 16)
    s.clear_line(16)  # clears specified line
    s.clear_line(17)  # clears specified line
    cursor_x, cursor_y = cur.get_location()
    current_row += 1
    cur.locate(col_1, current_row)
    prnt(label_color + "No text should appear between the red lines below.")
    current_row += 1
    cur.locate(col_1, current_row)
    prnt(label_color + "Clear Line Position:")
    cur.locate(col_2, current_row)
    prnt(field_color + "x=" + str(cursor_x) + ", y=" + str(cursor_y))
    prnt(note_color + "   (should be x=0, y=17)")

    # Demo locating a specific column
    cur.locate(0, 1)
    cur.locate_column(70)
    cursor_x, cursor_y = cur.get_location()
    current_row += 1
    cur.locate(col_1, current_row)
    prnt(label_color + "Locate Column Position:")
    cur.locate(col_2, current_row)
    prnt(field_color + "x=" + str(cursor_x) + ", y=" + str(cursor_y))
    prnt(note_color + "   (should be x=70, y=1)")

    # Demo locating the next line
    cur.locate(70, 10)
    cur.next_line(2)
    cursor_x, cursor_y = cur.get_location()
    current_row += 1
    cur.locate(col_1, current_row)
    prnt(label_color + "Next Line Column Position:")
    cur.locate(col_2, current_row)
    prnt(field_color + "x=" + str(cursor_x) + ", y=" + str(cursor_y))
    prnt(note_color + "   (should be x=0, y=12)")

    # Demo locating the previous line
    cur.locate(70, 10)
    cur.previous_line(2)
    cursor_x, cursor_y = cur.get_location()
    current_row += 1
    cur.locate(col_1, current_row)
    prnt(label_color + "Previous Line Column Position:")
    cur.locate(col_2, current_row)
    prnt(field_color + "x=" + str(cursor_x) + ", y=" + str(cursor_y))
    prnt(note_color + "    (should be x=0, y=8)")

    pause()

if __name__ == "__main__":
    main()
