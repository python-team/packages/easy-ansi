# Easy ANSI

Easy ANSI is a terminal framework API to give you an easy way to use colors,
cursor control movements, and line/box drawing. It is not meant as a
replacement to more full-featured frameworks
(such as [curses](https://docs.python.org/3/library/curses.html)
or [urwid](https://urwid.org/)),
but as a tool to quickly create nice-looking screens in your terminal window.
You can even create animations with the cursor controls.

**UPDATE**: Windows 10 does have support for ANSI sequences, but you must turn it on.
See below for details.

You can find demo programs [here](https://gitlab.com/joeysbytes/easy-ansi/-/tree/master/demos).

## Table of Contents

* [Requirements](#requirements)
* [General Usage](#general-usage)
* [Quick Reference](#quick-reference)
  * [Screen Commands](#screen-commands)
  * [Cursor Commands](#cursor-commands)
  * [Standard Colors](#standard-colors)
  * [Attributes](#attributes)
  * [256 Color Palette](#256-color-palette)
  * [RGB Color Palette](#rgb-color-palette)
  * [Drawing Lines and Boxes](#drawing-lines-and-boxes)
* [Color Reference](#color-reference)
  * [Standard Color Reference](#standard-color-reference)

<a id="requirements"></a>
## Requirements

* This library is not dependent on any operating system, but is instead
dependent on the capabilities of the terminal / terminal software you use.
* This library does not depend on any other external libraries.
* The Windows 10 Command Prompt and PowerShell prompt do support ANSI codes, but you must
enable it.
  1. Open up the Registry Editor
  1. Navigate to HKEY_CURRENT_USER\Console\
  1. Create an entry "VirtualTerminalLevel" if it does not exist.
  1. Set this entry to a DWORD value of 1.
* Other Windows terminal software will work with ANSI, such as Cygwin, Git Bash, PuTTY, etc.
* Linux / BSD will be the most supported environments for this library because of
their first-class support of ANSI.
* I do not have a Mac of any kind to test Easy ANSI against, but because OS X is a BSD-based
operating system, I assume Easy ANSI will work well on Apple computers.

<a id="general-usage"></a>
## General Usage

* All ANSI sequences come as either a command or a code (string). For
example, to clear the screen, you can call the *clear()* method, but
if you want to store the command to clear the screen, call the
*clear_code()* method instead.
* Certain ANSI sequences may not work as expected when combined with other
ANSI sequences. You should always visually check your application.
  * Mixing color palettes on the same line does not seem to work.
  * Not every terminal supports every attribute. You can check how attributes will display
  in the demo program *colors_demo.py*.
* Certain terminal settings can change the behavior of certain ANSI codes.
For example, many terminal emulators will display bright text as bold.
You will have to experiment with your terminal settings for best results.

<a id="quick-reference"></a>
## Quick Reference

<a id="screen-commands"></a>
### Screen Commands

```python
from easyansi import screen
```

| Method | Description | Parameters |
| --- | --- | --- |
| clear() | Clear the screen and move the cursor to 0, 0 | |
| clear_line(row) | Clear the line at the given row. The cursor will move to the beginning of the row cleared. | **row:** The row number to clear. |
| get_size() | Return the size of the terminal. If the system cannot determine the size of the terminal, a default size of 80 columns by 24 rows is returned. | |
| prnt(text) | A convenience method to print output to the screen without an ending newline character. | **text:** The text to output to the screen. |

<a id="cursor-commands"></a>
### Cursor Commands

```python
from easyansi import cursor
```

| Method | Description | Parameters |
| --- | --- | --- |
| locate(x, y) | Move the cursor to the x, y coordinates given. The upper-left corner is 0, 0. | **x:** column<br />**y:** row |
| locate_column(x) | Move the cursor to column x on the current line | **x:** column |
| get_location() | Return the x, y coordinates of where the cursor is. | |
| down(rows) | Move the cursor down a certain number of rows (default = 1). | **rows:** The number of rows to move the cursor down. |
| up(rows) | Move the cursor up a certain number of rows (default = 1). | **rows:** The number of rows to move the cursor up. |
| left(cols) | Move the cursor left a certain number of columns (default = 1). | **cols:** The number of columns to move the cursor left. |
| right(cols) | Move the cursor right a certain number of columns (default = 1). | **cols:** The number of columns to move the cursor right. |
| next_line(rows_down) | Move the cursor to the beginning of rows_down row (default = 1). | **rows_down:** The number of rows down to go to the beginning of. | 
| previous_line(rows_up) | Move the cursor to the beginning of rows_up row (default = 1). | **rows_up:** The number of rows up to go to the beginning of. |
| hide() | Hide the cursor. | |
| show() | Show the cursor. | |

<a id="standard-colors"></a>
### Standard Colors

See the color reference below.

```python
from easyansi import colors
```

| Method | Description | Parameters |
| --- | --- | --- |
| color(fg, bg) | Set the foreground and/or background color from the 16-color palette. | **fg:** Foreground color index.<br />**bg:** Background color index. |
| default(fg, bg) | Set the foreground and/or background color to the terminal default. | **fg:** Foreground color 0 (false) or 1 (true).<br />**bg:** Background color 0 (false) or 1 (true). |
| reset() | Reset the colors and attributes to terminal defaults. | |

<a id="attributes"></a>
### Attributes

```python
from easyansi import attributes
```

| Method | Description | Parameters |
| --- | --- | --- |
| normal()<br />bright()<br />dim() | Set the text intensity to one of normal, bright, or dim.<br />**NOTE:** Bright is often the same as bold text. | |
| italic()<br />italic_off() | Set italic text on or off. | |
| underline()<br />underline_off() | Set underline text on or off. | |
| reverse()<br />reverse_off() | Set reverse text on or off. | |
| strikethrough()<br />strikethrough_off() | Set strike-through text on or off. | |
| blink()<br />blink_off() | Set blinking text on or off. | |
| conceal()<br />conceal_off() | Set concealed (hidden) text on or off.<br />**WARNING:** Do NOT use this for password or other sensitive fields. Your text is still available to be copied from the terminal. | |
| reset() | Reset the colors and attributes to terminal defaults. | |

<a id="256-color-palette"></a>
### 256 Color Palette

See the color reference below.

```python
from easyansi import colors_256
```

| Method | Description | Parameters |
| --- | --- | --- |
| color(fg, bg) | Set the foreground and/or background color from the 256-color palette. | **fg:** Foreground color index.<br />**bg:** Background color index. |
| default(fg, bg) | Set the foreground and/or background color to the terminal default. | **fg:** Foreground color 0 (false) or 1 (true).<br />**bg:** Background color 0 (false) or 1 (true). |
| reset() | Reset the colors and attributes to terminal defaults. | |

<a id="rgb-color-palette"></a>
### RGB Color Palette

```python
from easyansi import colors_rgb
```

| Method | Description | Parameters |
| --- | --- | --- |
| color(r, g, b, bg_r, bg_g, bg_b) | Set the foreground and/or background color to the provided RGB values. | **r:** The foreground red value (0-255).<br />**g:** The foreground green value (0-255).<br />**b:** The foreground blue value (0-255).<br />**bg_r:** The background red value (0-255).<br />**bg_g:** The background green value (0-255).<br />**bg_b:** The background blue value (0-255). |
| reset() | Reset the colors and attributes to terminal defaults. | |

<a id="drawing-lines-and-boxes"></a>
### Drawing Lines and Boxes

```python
from easyansi.drawing import single
from easyansi.drawing import double
from easyansi.drawing import keyboard
```

| Drawing Style | Description |
| --- | --- |
| single | Single line drawing characters.
| double | Double line drawing characters.
| keyboard | Line drawing characters only from keyboard characters.

```python
from easyansi import drawing
```

| Method | Description | Parameters |
| --- | --- | --- |
| hline(length, char, style) | Draw a horizontal line to a given length, optionally composed of the given char(s) or of a given style. | **length:** Total length of the line.<br />**char:** The character(s) to use to build the line.<br />**style:** The line style to use to build the line. char will override the style. |
| vline(length, char, style) | Draw a vertical line to a given length, optionally composed of the given char(s) or of a given style. | **length:** Total length of the line.<br />**char:** The character(s) to use to build the line.<br />**style:** The line style to use to build the line. char will override the style. |
| box(width, height, style, top, bottom, left, right, top_left, top_right, bottom_left, bottom_right, top_bottom, left_right, all_corners, all_sides, all_chars) | Draw a box. Only width and height is required. This utilizes the hline and vline methods internally. | **width:** Width of the box.<br />**height:** Height of the box.<br />**style:** The line style to use.<br />**top:** The character(s) to use for the top line.<br />**bottom:** The character(s) to use for the bottom line.<br />**left:** The character(s) to use for the left line.<br />**right:** The character(s) to use for the right line.<br />**top_left:** The character to use for the top left corner.<br />**top_right:** The character to use for the top right corner.<br />**bottom_left:** The character to use for the bottom left corner.<br />**bottom_right:** The character to use for the bottom_right corner.<br />**top_bottom:** The character(s) to use for the top and bottom lines.<br />**left_right:** The character(s) to use for the left and right lines.<br />**all_corners:** The character to use for all the corners.<br />**all_sides:** The character(s) to use for all sides.<br />**all_chars:** The character to use for all drawing of the box. |

<a id="color-reference"></a>
## Color Reference

<a id="standard-color-reference"></a>
### Standard Color Reference

| Color Index | Color Name | Color Index | Color Name |
| --- | --- | --- | --- |
| 0 | BLACK | 8 | BRIGHT_BLACK |
| 1 | RED | 9 | BRIGHT_RED |
| 2 | GREEN | 10 | BRIGHT_GREEN |
| 3 | YELLOW |11 | BRIGHT_YELLOW |
| 4 | BLUE | 12 | BRIGHT_BLUE |
| 5 | MAGENTA | 13 | BRIGHT_MAGENTA |
| 6 | CYAN | 14 | BRIGHT_CYAN |
| 7 | WHITE | 15 | BRIGHT_WHITE |

![Screenshot of standard colors](https://gitlab.com/joeysbytes/easy-ansi/-/raw/master/docs/images/color_chart.png)
