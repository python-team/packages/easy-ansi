#!/bin/bash

# This script is used for running the PyTest tests

source ./config.sh

# This can be a directory
PYTEST_FILE=""

if [ $# -ne 0 ]
then
    if [ $# -eq 1 ]
    then
        PYTEST_FILE="${1}"
        if [ ! -e "${TESTS_DIR}/${PYTEST_FILE}" ]
        then
            echo "PyTest file or dir not found: ${TESTS_DIR}/${PYTEST_FILE}"
            exit 1
        fi
    else
        echo "Too many parameters, only 1 optionally allowed."
        exit 1
    fi
fi

"${CONDA_BIN_DIR}/activate" "${CONDA_DEV_ENV}"
cd "${DEV_DIR}"
pip install -e .
echo ""
echo "----- PyTest Starts Below -----"

# -s(--capture=no): let's sysout show on screen
# --verbose
python -m pytest --rootdir="${TESTS_DIR}" "${TESTS_DIR}/${PYTEST_FILE}"
