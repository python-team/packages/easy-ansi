# Variables for developer scripts to utilize

CONDA_BIN_DIR="/work/miniconda/bin"
CONDA_DEV_ENV="easy-ansi_env"
DEV_DIR="/data/joey/development/easy-ansi"

SRC_DIR="${DEV_DIR}/easyansi"
TESTS_DIR="${DEV_DIR}/tests"
DEMOS_DIR="${DEV_DIR}/demos"
BUILD_DIR="${DEV_DIR}/build"
DIST_DIR="${DEV_DIR}/dist"
