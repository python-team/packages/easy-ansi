#!/bin/bash

# This script is used for running MyPy

source ./config.sh

# This can be a directory
MYPY_FILE=""

if [ $# -ne 0 ]
then
    if [ $# -eq 1 ]
    then
        MYPY_FILE="${1}"
        if [ ! -e "${SRC_DIR}/${MYPY_FILE}" ]
        then
            echo "Python file or dir not found: ${SRC_DIR}/${MYPY_FILE}"
            exit 1
        fi
    else
        echo "Too many parameters, only 1 optionally allowed."
        exit 1
    fi
fi

"${CONDA_BIN_DIR}/activate" "${CONDA_DEV_ENV}"
cd "${DEV_DIR}"
echo ""
echo "----- MyPy Starts Below -----"
python -m mypy --strict --python-version 3.6 "${SRC_DIR}/${MYPY_FILE}"
