#!/bin/bash

# This script is used for running the demo programs

source ./config.sh

DEMO_FILE="${1}"

if [ $# -ne 1 ]
then
    echo "Pass in name of demo to run."
    exit 1
fi
if [ ! -f "${DEMOS_DIR}/${1}" ]
then
    echo "Demo file not found: ${1}"
    exit 1
fi

"${CONDA_BIN_DIR}/activate" "${CONDA_DEV_ENV}"
cd "${DEV_DIR}"
pip install -e .
cd "${DEMOS_DIR}"
echo ""
echo "----- Demo Starts Below -----"
python "${DEMO_FILE}"
