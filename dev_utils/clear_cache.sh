#!/bin/bash

source ./config.sh

DELETE_CACHE="no"

if [ $# -ne 0 ]
then
    if [ $# -eq 1 ]
    then
        if [ "${1}" == "yes" ]
        then
            DELETE_CACHE="yes"
        else
            echo "Deletion argument must be 'yes'"
            exit 1
        fi
    else
        echo "Too many parameters, only 1 optionally allowed."
        exit 1
    fi
fi

dir_list=""

function main() {
    find_dir ".mypy_cache"
    find_dir ".pytest_cache"
    find_dir "build"
    find_dir "dist"
    find_dir "easy_ansi.egg-info"
    delete_dirs
}

# Parameter 1 = directory name to find
function find_dir() {
    local directory_list=$(find "${DEV_DIR}" -name "${1}" -type d)
    dir_list="${dir_list}${directory_list}"
    dir_list="${dir_list}"$'\n'
}

function delete_dirs() {
    while IFS= read -r directory
    do
        dirname_length=${#directory}
        if [ ! "${dirname_length}" == "0" ]
        then
            if [ "${DELETE_CACHE}" == "yes" ]
            then
                echo "Deleting: ${directory}"
                rm -rf "${directory}"
            else
                echo "Found: ${directory}"
            fi
        fi
    done <<< "${dir_list}"
}

main