# Release Checklist

* Update Documentation
  * Main README.md
  * Demos README.md
  * PyDocs

* Update Version Number / setup.py
* Update Changelog

* Merge code to master
* tag code
* Publish to PyPi

* Generate / Publish PyDocs
