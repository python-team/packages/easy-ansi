# Change Log

## v0.3

**Date:** 2020-03-04

**New Features:**

* cursor: locate_column(x) - move the cursor to a specified column (x) on the current row.
* cursor: next_line(rows_down) - move the cursor to the beginning of the line which is rows_down.
* cursor: previous_line(rows_up) - move the cursor to the beginning of the line which is rows_up.

**Fixed:**

* Remove cursor.get_location() dependency from screen.clear_line(). The flexibility this was meant to allow did not work as intended. The row number must be provided in the method call.
* Documentation fixes.

## v0.2.2

**Date:** 2020-03-02

**Fixed:**

* cursor.locate() depended on cursor.get_location() for flexibility, but this flexibility did not work as intended. Removed dependency, and both x and y values are required again.

## v0.2.1

**Date:** 2020-02-27

**Fixed:**

* cursor.get_location() was causing EasyANSI as a whole to not work on Windows.

## v0.2

**Date:** 2020-02-23

**New Features:**

* screen: clear_line(row) - will now clear the line the cursor is on if you do not provide a specific row to clear.
* cursor: get_location() - a new function that will return the x, y coordinates of the cursor.
* cursor: locate(x, y) - only one of x or y is now required, the other value, if not supplied, will use the value of the cursor location. 

## v0.1

**Date:** 2020-02-22

Initial Release.
